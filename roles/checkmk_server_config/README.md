# Configure CheckMK Server

This module creates and configures a CheckMK site on your target monitoring server.

# The below is set in group_vars/all.yml - as some tasks need to run as root user, this can be added in individual tasks instead.
- ansible_become: yes
- ansible_user: root
-----------------------------------------------------------------------------------------
# Required collections that need to be installed on the ansible controller, or the machine you're running ansible from.
Install checkmk.general, this will also install community.general & ansible.posix as dependancies.
- ansible-galaxy collection install checkmk.general
-----------------------------------------------------------------------------------------
# Requires Variables
- checkmk_server_config/vars/main.yml - Edit the variables in this file to suit your specific setup.
  - checkmk_server_site: This is the site name for your CMK site, you will be able to sudo su - sitename to manage OMD.
  - checkmk_server_download_url: This url can be found from the official download page for CheckMK https://checkmk.com/download
  - checkmk_server_download_checksum: The checksum for the download can be found below the download link on the same page above.
  - checkmk_server_version: Make sure this is the version number of the URL you use to install CMK.
  - checkmk_server_download: This will be the full name of the package you download.
  - checkmk_server_adminpw: Define this var using ansible vault to encrypt the cmkadmin password string. 
Check sample-variables.yml for examples
-----------------------------------------------------------------------------------------
# Defaults
- checkmk_server_config/defaults.main.yml
Leave defaults as is, if there is a reason to change anything in here be careful as it may break the main playbook.
All of the tasks/main.yml vars point to here. 
-----------------------------------------------------------------------------------------
# Additional info
The target server needs to have 3 python modules present. passlib, setuptools_rust & bcrypt. 
This is taken care of via the ansible pip module in tasks/main.yml and is required for the automation of setting the cmkadmin password.
